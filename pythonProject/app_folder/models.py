from django.db import models


class strDB(models.Model):
    class Meta:
        db_table = 'str_table'
        verbose_name_plural = 'str_table'

    str_id = models.IntegerField('id', null=True, blank=True, )
    str = models.CharField('str', max_length=100, null=True, blank=True, )


class GoodReturnDB(models.Model):
    class Meta:
        db_table = 'good_return_table'
        verbose_name_plural = 'good_return_table'

    good_id = models.IntegerField('id', null=True, blank=True, )
    good_return_str = models.CharField('good_return_str', max_length=100, null=True, blank=True, )


class BatReturnDB(models.Model):
    class Meta:
        db_table = 'bat_return_table'
        verbose_name_plural = 'bat_return_table'

    bat_id = models.IntegerField('id', null=True, blank=True, )
    bat_return_str = models.CharField('bat_return_str', max_length=100, null=True, blank=True, )
