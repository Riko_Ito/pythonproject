from django.contrib import admin
from .models import GoodReturnDB, BatReturnDB, strDB  # models.pyで指定したクラス名


admin.site.register(GoodReturnDB)  # models.pyで指定したクラス名
admin.site.register(BatReturnDB)
admin.site.register(strDB)
