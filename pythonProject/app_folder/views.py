import random

from django.shortcuts import render
from django.views import View
from .models import GoodReturnDB, strDB
from .models import BatReturnDB


class TopView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'app_folder/top.html')

    def post(self, request, *args, **kwargs):
        # メッセージ格納
        result = strDB.objects.filter(id=3)
        result_choice = result[0].str
        context = {'result_choice': result_choice}

        return render(request, 'app_folder/choice.html', context=context, )


top_page = TopView.as_view()


class PostView(View):
    def get(self, request, *args, **kwargs):
        result = strDB.objects.filter(id=1)
        result_str = result[0].str
        context = {'result_str': result_str}
        # セッションに保存する
        request.session['thing'] = "good_thing"

        return render(request, 'app_folder/form.html', context=context, )


post_page = PostView.as_view()


class BatPostView(View):
    def get(self, request, *args, **kwargs):
        result = strDB.objects.filter(id=2)
        result_str = result[0].str
        context = {'result_str': result_str}
        # セッションに保存する
        request.session['thing'] = "bat_thing"
        return render(request, 'app_folder/form.html', context=context, )


bat_post_page = BatPostView.as_view()


class ToTopView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'app_folder/to_top.html')

    def post(self, request, *args, **kwargs):
        return render(request, 'app_folder/top.html')


return_top_page = ToTopView.as_view()


class GoodReturnView(View):
    def get(self, request, *args, **kwargs):
        i = random.randint(1, 6)
        result = GoodReturnDB.objects.filter(id=i)
        result_return = result[0].good_return_str
        context = {'result_return': result_return}
        return render(request, 'app_folder/return.html', context=context, )


good_return_page = GoodReturnView.as_view()


class NextChoiceView(View):
    def get(self, request, *args, **kwargs):
        # セッションから選択ボタン情報削除
        del request.session['thing']
        # メッセージ格納
        result = strDB.objects.filter(id=4)
        result_choice = result[0].str
        context = {'result_choice': result_choice}

        return render(request, 'app_folder/choice.html', context=context, )


next_choice_page = NextChoiceView.as_view()


class BatReturnView(View):
    def get(self, request, *args, **kwargs):
        i = random.randint(1, 6)
        result = BatReturnDB.objects.filter(id=i)
        result_return = result[0].bat_return_str
        context = {'result_return': result_return}
        return render(request, 'app_folder/return.html', context=context, )


bat_return_page = BatReturnView.as_view()
