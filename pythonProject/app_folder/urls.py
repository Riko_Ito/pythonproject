from django.urls import path
from . import views

app_name = 'app_folder'
urlpatterns = [
    path('top_page/', views.top_page, name='top_page'),
    path('post_page/', views.post_page, name='post_page'),
    path('bat_post_page/', views.bat_post_page, name='bat_post_page'),
    path('return_top_page/', views.return_top_page, name='return_top_page'),
    path('good_return_page/', views.good_return_page, name='good_return_page'),
    path('next_choice_page/', views.next_choice_page, name='next_choice_page'),
    path('bat_return_page/', views.bat_return_page, name='bat_return_page'),
]
